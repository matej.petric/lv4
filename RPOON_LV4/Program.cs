﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class Program
    {
        static void Main(string[] args)
        {
            //PRVI
            //double[][] myArrays = new double[3][];
            //double[] firstArray = new double[3] { 1, 2, 3 };
            //double[] secondArray = new double[3] { 4, 5, 6 };
            //double[] thirdArray = new double[3] { 7, 8, 9 };
            //myArrays[0] = firstArray;
            //myArrays[1] = secondArray;
            //myArrays[2] = thirdArray;

            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            //double[] averageByRow1 = analyzer.PerRowAverage(myArrays);
            //for (int i = 0; i < averageByRow1.Length; i++)
            //{
            //    Console.Write(averageByrow1[i] + " ");
            //}
            //double[] averageByColumn1 = analyzer.PerColumnAverage(myArrays);
            //for (int i = 0; i < averageByColumn1.Length; i++)
            //{
            //    Console.Write(averageByColumn1[i] + " ");
            //}

            //DRUGI
            Dataset myDataset = new Dataset(@"C:\Users\student\Desktop\csv.txt");
            Adapter myAdapter = new Adapter(analyzer);
            double[] averageByRow2 = myAdapter.CalculateAveragePerRow(myDataset);
            Console.WriteLine("Average by row:");
            for(int i=0;i<averageByRow2.Length;i++)
            {
                Console.Write(averageByRow2[i] + " ");
            }

            double[] averageByColumn2 = myAdapter.CalculateAveragePerColumn(myDataset);
            Console.WriteLine("\nAverage by column:");
            for (int i = 0; i < averageByColumn2.Length; i++)
            {
                Console.Write(averageByColumn2[i] + " ");
            }
            Console.WriteLine();

            //TRECI
            Console.WriteLine();
            List<IRentable> favourites = new List<IRentable>();
            favourites.Add(new Book("Biblija"));
            favourites.Add(new Video("The Godfather"));
            RentingConsolePrinter printer = new RentingConsolePrinter();
            printer.DisplayItems(favourites);

            //CETVRTI
            Console.WriteLine();
            favourites.Add(new HotItem(new Video("Avenđeri")));
            favourites.Add(new HotItem(new Book("50 nijansi Saleta")));
            printer.DisplayItems(favourites);

            //PETI
            Console.WriteLine();
            List<IRentable> flashSale = new List<IRentable>();
            foreach(IRentable rentable in favourites)
            {
                flashSale.Add(new DiscountedItem(rentable));
            }
            printer.DisplayItems(flashSale);

            //SESTI
            EmailValidator emailValidator = new EmailValidator();
            string example1 = "me@example.com";
            string example2 = "me@example.hr";
            string example3 = "me@example.coom";
            string example4 = "meexample.com";
            Console.WriteLine("\n" + emailValidator.IsValidAddress(example1));
            Console.WriteLine(emailValidator.IsValidAddress(example2));
            Console.WriteLine(emailValidator.IsValidAddress(example3));
            Console.WriteLine(emailValidator.IsValidAddress(example4));

            //SEDMI
            PasswordValidator passwordValidator = new PasswordValidator(6);
            RegistrationValidator registrationValidator = new RegistrationValidator(emailValidator, passwordValidator);
            UserEntry userEntry = null;
            do
            {
                userEntry = UserEntry.ReadUserFromConsole();
            }
            while (!registrationValidator.IsUserEntryValid(userEntry));
        }
    }
}
