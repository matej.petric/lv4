﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class Adapter : IAnalytics
    {
        private Analyzer3rdParty analyticsService;
        public Adapter(Analyzer3rdParty service)
        {
            this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            IList<List<double>> dataToConvert = dataset.GetData();
            double[][] converted = new double[dataToConvert.Count][];

            for(int i=0;i<dataToConvert.Count;i++)
            {
                double[] row = new double[dataToConvert[i].Count];
                for(int j=0;j<dataToConvert[0].Count;j++)
                {
                    row[j] = dataToConvert[i][j]; 
                }
                converted[i] = row;
            }
            return converted;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }
    }
}
