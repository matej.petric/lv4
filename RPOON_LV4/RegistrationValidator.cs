﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class RegistrationValidator : IRegistrationValidator
    {
        private EmailValidator emailValidator = new EmailValidator();
        private PasswordValidator passwordValidator = new PasswordValidator(6);

        public RegistrationValidator(EmailValidator EmailValidator, PasswordValidator PasswordValidator)
        {
            emailValidator = EmailValidator;
            passwordValidator = PasswordValidator;
        }

        public bool IsUserEntryValid(UserEntry userEntry)
        {
            return emailValidator.IsValidAddress(userEntry.Email) && passwordValidator.IsValidPassword(userEntry.Password);
        }
    }
}
