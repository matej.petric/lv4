﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class DiscountedItem : RentableDecorator
    {
        private readonly double Discount = 0.25;
        public DiscountedItem(IRentable rentable): base(rentable) { }
        public override double CalculatePrice()
        {
            return base.CalculatePrice() * (1 - this.Discount);
        }
        public override string Description
        {
            get
            {
                return base.Description+", now at "+ this.Discount + "% off! \n Price: "+CalculatePrice().ToString();
            }
        }
    }
}
