﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class Analyzer3rdParty
    {
        public double[] PerRowAverage(double[][] data)
        {
            int rowCount = data.Length;
            double[] results = new double[rowCount];
            for (int i = 0; i < rowCount; i++)
            {
                results[i] = data[i].Average();
            }
            return results;
        }

        public double[] PerColumnAverage(double[][] data)
        {
            double[][] transponedData = new double[data[0].Length][];
            for(int i=0;i<data[0].Length;i++)
            {
                transponedData[i] = new double[data.Length];
                for(int j=0;j<data.Length;j++)
                {
                    transponedData[i][j] = data[j][i];
                }
            }
            return PerRowAverage(transponedData);
        }
    }
}
