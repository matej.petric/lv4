﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPOON_LV4
{
    class Book : IRentable
    {
        private readonly double BaseBookPrice = 3.99;
        public string Title { get; private set; }
        public Book(string Title) { this.Title = Title; }
        public string Description { get { return this.Title; } }
        public double CalculatePrice() { return BaseBookPrice; }
    }
}
